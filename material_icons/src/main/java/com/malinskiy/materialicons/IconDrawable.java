package com.malinskiy.materialicons;

import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.ColorFilter;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

import static java.lang.String.valueOf;
import static ohos.agp.utils.TextAlignment.CENTER;

/**
 * Embed an icon into a Drawable that can be used as TextView icons, or ActionBar icons.
 * <p/>
 * <pre>
 *     new IconDrawable(context, IconValue.icon_star)
 *           .colorRes(R.color.white)
 *           .actionBarSize();
 * </pre>
 * If you don't set the size of the drawable, it will use the size
 * that is given to him. Note that in an ActionBar, if you don't
 * set the size explicitly it uses 0, so please use actionBarSize().
 */
public class IconDrawable extends ShapeElement {

    public static final int ACTIONBAR_ICON_SIZE_VP = 24;
    public static final int DEFAULT_ALPHA = 255;

    private final Context context;

    private final Iconify.IconValue icon;

    private Paint paint;

    private int size = -1;
    private int alpha = DEFAULT_ALPHA;

    /**
     * Create an IconDrawable.
     *
     * @param component Your activity or application context.
     * @param icon    The icon you want this drawable to display.
     */
    public IconDrawable(Component component, Iconify.IconValue icon) {
        this.context = component.getContext();
        this.icon = icon;
        paint = new Paint();
        paint.setFont(Iconify.getTypeface(context));
        paint.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
        paint.setTextAlign(CENTER);
        paint.setColor(Color.BLACK);
        paint.setAntiAlias(true);
        component.addDrawTask((component1, canvas) -> drawToCanvas(canvas));
    }

    /**
     * Set the size of this icon to the ActionBar.
     *
     * @return The current IconDrawable for chaining.
     */
    public IconDrawable actionBarSize() {
        return sizeVp(ACTIONBAR_ICON_SIZE_VP);
    }

    /**
     * Set the size of the drawable.
     *
     * @param floatRes The dimension resource.
     * @return The current IconDrawable for chaining.
     */
    public IconDrawable sizeRes(int floatRes) {
        float value = 0;
        try {
            value = context.getResourceManager().getElement(floatRes).getFloat();
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return sizePx((int) value);
    }

    /**
     * Set the size of the drawable.
     *
     * @param size The size in density-independent pixels (dp).
     * @return The current IconDrawable for chaining.
     */
    public IconDrawable sizeVp(int size) {
        return sizePx(Utils.convertVpToPx(context, size));
    }

    /**
     * Set the size of the drawable.
     *
     * @param size The size in pixels (px).
     * @return The current IconDrawable for chaining.
     */
    public IconDrawable sizePx(int size) {
        this.size = size;
        setBounds(0, 0, size, size);
        return this;
    }

    /**
     * Set the color of the drawable.
     *
     * @param color The color, usually from agp.utils.Color or 0xFF012345.
     * @return The current IconDrawable for chaining.
     */
    public IconDrawable color(int color) {
        paint.setColor(new Color(color));
        return this;
    }

    /**
     * Set the color of the drawable.
     *
     * @param colorRes The color resource, from your R file.
     * @return The current IconDrawable for chaining.
     */
    public IconDrawable colorRes(int colorRes) {
        try {
            paint.setColor(new Color(context.getResourceManager().getElement(colorRes).getColor()));
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return this;
    }

    /**
     * Set the alpha of this drawable.
     *
     * @param alpha The alpha, between 0 (transparent) and 255 (opaque).
     * @return The current IconDrawable for chaining.
     */
    public IconDrawable alpha(int alpha) {
        setAlpha(alpha);
        return this;
    }

    public int getIntrinsicHeight() {
        return size;
    }

    public int getIntrinsicWidth() {
        return size;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void drawToCanvas(Canvas canvas) {
        paint.setTextSize(getBounds().getHeight());
        String textValue = valueOf(icon.character());
        Rect textBounds = paint.getTextBounds(textValue);
        float textBottom = (getBounds().getHeight() - textBounds.getHeight()) / 2f + textBounds.getHeight() - textBounds.bottom;
        canvas.drawText(paint, textValue, getBounds().getWidth() / 2f, textBottom);
    }

    @Override
    public boolean isStateful() {
        return true;
    }

    public boolean setState(int[] stateSet) {
        float oldValue = paint.getAlpha();
        int newValue = Utils.isEnabled(stateSet) ? alpha : alpha / 2;
        paint.setAlpha(newValue);
        return oldValue != newValue;
    }

    @Override
    public void setAlpha(int alpha) {
        this.alpha = alpha;
        paint.setAlpha(alpha);
    }

    public void setColorFilter(ColorFilter cf) {
        paint.setColorFilter(cf);
    }

    public void clearColorFilter() {
        paint.setColorFilter(null);
    }

    public int getOpacity() {
        return this.alpha;
    }

    /**
     * Sets paint style.
     *
     * @param style to be applied
     */
    public void setStyle(Paint.Style style) {
        paint.setStyle(style);
    }

}
