package com.malinskiy.materialicons;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.ComponentState;
import ohos.app.Context;
import ohos.app.Environment;

import java.io.*;

import static com.malinskiy.materialicons.Iconify.IconValue;

final class Utils {

    public static final String ICON_FONT_FOLDER = "resources/rawfile/";
    public static final String NAME_PREFIX      = "{zmdi";

    private Utils() {
        // Prevents instantiation
    }

    static int convertVpToPx(Context context, float vp) {
        return AttrHelper.vp2px(vp, context);
    }

    static boolean isEnabled(int[] stateSet) {
        for (int state : stateSet)
            if (state == ComponentState.COMPONENT_STATE_DISABLED)
                return false;
        return true;
    }

    static File resourceToFile(Context context, String resourceName) throws IOException {

        File outPath = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), resourceName);
        if (outPath.exists()) return outPath;

        BufferedOutputStream bos = null;
        InputStream inputStream = null;
        try {
            inputStream = context.getResourceManager().getRawFileEntry(ICON_FONT_FOLDER + resourceName).openRawFile();
            byte[] buffer = new byte[inputStream.available()];
            bos = new BufferedOutputStream(new FileOutputStream(outPath));
            int l = 0;
            while ((l = inputStream.read(buffer)) > 0) {
                bos.write(buffer, 0, l);
            }
            return outPath;
        } finally {
            closeQuietly(bos);
            closeQuietly(inputStream);
        }
    }

    private static void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                // Don't care
            }
        }
    }

    public static StringBuilder replaceIcons(StringBuilder text) {
        int startIndex = text.indexOf(NAME_PREFIX);
        if (startIndex == -1) {
            return text;
        }

        int endIndex = text.substring(startIndex).indexOf("}") + startIndex + 1;

        String iconString = text.substring(startIndex + 1, endIndex - 1);
        iconString = iconString.replaceAll("-", "_");
        try {

            IconValue value = IconValue.valueOf(iconString);
            String iconValue = String.valueOf(value.character());

            text = text.replace(startIndex, endIndex, iconValue);
            return replaceIcons(text);

        } catch (IllegalArgumentException e) {
            LogUtils.w(Iconify.TAG, "Wrong icon name: " + iconString);
            return text;
        }
    }
}
