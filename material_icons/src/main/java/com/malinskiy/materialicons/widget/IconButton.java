package com.malinskiy.materialicons.widget;

import com.malinskiy.materialicons.Iconify;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Button;
import ohos.app.Context;

public class IconButton extends Button {

    public IconButton(Context context) {
        super(context);
        init(null);
    }

    public IconButton(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public IconButton(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }


    private void init(AttrSet attrs) {
        Iconify.addIcons(this);
    }

    @Override
    public void setText(String text) {
        super.setText(Iconify.compute(text));
    }

}
