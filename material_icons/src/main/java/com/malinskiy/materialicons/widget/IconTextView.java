package com.malinskiy.materialicons.widget;

import com.malinskiy.materialicons.Iconify;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Text;
import ohos.app.Context;

public class IconTextView extends Text {

    public IconTextView(Context context) {
        super(context);
        init(null);
    }

    public IconTextView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public IconTextView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    private void init(AttrSet attrs) {
        Iconify.addIcons(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setText(String text) {
        super.setText(Iconify.compute(text));
    }

}
