> **[All available icons (777)](http://zavoloklom.github.io/material-design-iconic-font/icons.html)**

**If, like me, you're tired of copying 5 images for each icon you want to use in your app, for each color you want to use them with ```material-icons``` can help you.**

-----

### About

**material-icons** allows you to include any of the **[Material Design 2.1.1 icons by Google packed by Sergey Kupletsky](http://zavoloklom.github.io/material-design-iconic-font/)** in your texts, your ```ActionBar```, and even in your ```EditText```s. Icons are infinitely **scalable**, and **customizable** with shadows and everything you can do on texts.

### Get started #1

If you need icons on a ```TextView```, use the ```{ }``` syntax. You can put any text around it and have more than one icon in the text. Note that the shadows apply to the icons as well.

```xml
<IconTextView
    ohos:id="$+id:item_text"
    ohos:width="match_parent"
    ohos:height="match_content"
    ohos:top_margin="10vp"
    ohos:padding="4vp"
    ohos:text_size="40fp"
    ohos:text_color="#cd0716"
    ohos:max_text_lines="2"
    ohos:below="$id:item_icon"
    ohos:text_alignment="center"
    ohos:text="Sample icon"
    ... />
```

> You can either use ```IconTextView``` / ```ButtonTextView``` or use any ```TextView``` and then programmatically call ```Iconify.addIcons(myTextView);```.

### Get started #2

If you need an icon in an ```ImageView``` or in your ```ActionBar```, then you should use ```IconDrawable```. Again, icons are infinitely scalable and will never get fuzzy!

```java
// Set an icon in the ActionBar
Image iconView = (Image) findComponentById(ResourceTable.Id_item_icon);
Iconify.IconValue iconValue = Iconify.IconValue.values()[position];
IconDrawable element = new IconDrawable(iconView, iconValue)
                .sizeRes(ResourceTable.Float_icon_detailed_size)
                .alpha(100)
                .colorRes(ResourceTable.Color_detail_color);
             element.setColorFilter(new ColorFilter(getColor(ResourceTable.Color_filter_color), BlendMode.COLOR));
             element.setStyle(Paint.Style.FILL_STYLE);
      iconView.setImageElement(element);
```

-----
## Get it

Gradle:
```groovy
repositories {
    ...
    mavenCentral()
    ...
}
...
dependencies {
    ...
    compile 'io.github.dzsf:material-icons:1.0.0'
    ...
}
```

## License

```
Copyright 2013 Joan Zapata
Copyright 2014 Anton Malinskiy

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

It uses Google's material font licensed under OFL 1.1, which is compatible
with this library's license.

    http://scripts.sil.org/cms/scripts/render_download.php?format=file&media_id=OFL_plaintext&filename=OFL.txt
    
```

## Screenshots

<img src="./Screenshot20210708161339874.png" width="400">

 

<img src="./Screenshot20210708161243696.png" width="400" >