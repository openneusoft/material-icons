package com.malinskiy.materialicons.sample;

import com.malinskiy.materialicons.IconDrawable;
import com.malinskiy.materialicons.Iconify;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class IconsAdapter extends BaseItemProvider {

    private Context mContext;

    public IconsAdapter(Context context) {
        mContext = context;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCount() {
        return Iconify.IconValue.values().length;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getItem(int i) {
        return Iconify.IconValue.values()[i];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getItemId(int i) {
        return i;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        ViewHolder holder;
        Component retComponent;
        if (component == null) {
            holder = onCreateViewHolder(componentContainer);
            retComponent = holder.itemView;
        } else {
            holder = (ViewHolder) component.getTag();
            retComponent = component;
        }
        onBindViewHolder(holder, position);
        retComponent.setTag(holder);
        return retComponent;
    }

    private ViewHolder onCreateViewHolder(ComponentContainer parent) {
        Component view = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item_icon, parent, false);
        Image textView = (Image) view.findComponentById(ResourceTable.Id_item_icon);
        Text nameView = (Text) view.findComponentById(ResourceTable.Id_item_text);

        ViewHolder viewHolder = new ViewHolder(view, textView, nameView);
        return viewHolder;
    }

    private void onBindViewHolder(ViewHolder holder, int position) {
        Iconify.IconValue iconValue = Iconify.IconValue.values()[position];
        holder.iconView.setBackground(
                new IconDrawable(holder.iconView, iconValue)
                        .sizeRes(ResourceTable.Float_icon_size)
                        .color(Color.DKGRAY.getValue()));
        Iconify.setIcon(holder.nameView, iconValue);
    }

    public static class ViewHolder {
        public Component itemView;
        public Image iconView;
        public Text nameView;

        public ViewHolder(Component itemView, Image iconView, Text nameView) {
            this.itemView = itemView;
            this.iconView = iconView;
            this.nameView = nameView;
        }
    }

}
