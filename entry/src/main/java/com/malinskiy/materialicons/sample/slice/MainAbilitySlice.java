/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.malinskiy.materialicons.sample.slice;

import com.malinskiy.materialicons.sample.IconsAdapter;
import com.malinskiy.materialicons.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.TableLayoutManager;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class MainAbilitySlice extends AbilitySlice implements ListContainer.ItemClickedListener {
    private ListContainer recyclerView;
    private BaseItemProvider adapter;
    private TableLayoutManager layoutManager;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        setupViews();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onActive() {
        super.onActive();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
        present(new IconDetailsActivitySlice(), IconDetailsActivitySlice.createIntent(i));
    }
    /**
     * setup view
     */
    public void setupViews() {
        recyclerView = (ListContainer) findComponentById(ResourceTable.Id_recycler_icons);

        layoutManager = new TableLayoutManager();
        try {
            layoutManager.setColumnCount(getResourceManager().getElement(ResourceTable.Integer_icon_columns).getInteger());
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemClickedListener(this);

        adapter = new IconsAdapter(this);
        recyclerView.setItemProvider(adapter);

    }
}
