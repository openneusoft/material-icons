/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.malinskiy.materialicons.sample.slice;

import com.malinskiy.materialicons.IconDrawable;
import com.malinskiy.materialicons.Iconify;
import com.malinskiy.materialicons.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.render.BlendMode;
import ohos.agp.render.ColorFilter;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;

public class IconDetailsActivitySlice extends AbilitySlice {

    public static final String ARG_POSITION = "position";
    public static final int INT_ALPHA = 100;
    public static final int TEXT_SIZE = 100;

    private int position;
    private Image iconView;
    private Text nameView;
    private Button btnView;

    public static Intent createIntent(int position) {
        Intent intent = new Intent();
        intent.setParam(ARG_POSITION, position);
        return intent;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_icon_details_activity);
        this.position = intent.getIntParam(ARG_POSITION, 0);
        setup();
    }

    /**
     * setup component
     */
    public void setup() {
        iconView = (Image) findComponentById(ResourceTable.Id_item_icon);
        nameView = (Text) findComponentById(ResourceTable.Id_item_text);

        Iconify.IconValue iconValue = Iconify.IconValue.values()[position];
        IconDrawable element = new IconDrawable(iconView, iconValue)
                .sizeRes(ResourceTable.Float_icon_detailed_size)
                .alpha(INT_ALPHA)
                .colorRes(ResourceTable.Color_detail_color);
        element.setColorFilter(new ColorFilter(getColor(ResourceTable.Color_filter_color), BlendMode.COLOR));
        element.setStyle(Paint.Style.FILL_STYLE);
        iconView.setImageElement(element);

        nameView.setText("text:" + iconValue.formattedName());
        Iconify.addIcons(nameView);

        btnView = (Button) findComponentById(ResourceTable.Id_item_btnicon);
        Iconify.IconValue iconBtnValue = Iconify.IconValue.values()[position];
        IconDrawable element2 = new IconDrawable(btnView, iconBtnValue)
                .sizeRes(ResourceTable.Float_icon_detailed_size)
                .color(Color.RED.getValue())
                .alpha(INT_ALPHA)
                .colorRes(Color.YELLOW.getValue());
        element2.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
        btnView.setCursorElement(element2);
        btnView.setTextSize(TEXT_SIZE);
        btnView.setText("btn:" + iconBtnValue.formattedName());
        Iconify.addIconsEditMode(btnView);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onActive() {
        super.onActive();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

}
