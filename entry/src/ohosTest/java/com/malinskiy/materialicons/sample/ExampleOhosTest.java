package com.malinskiy.materialicons.sample;

import com.malinskiy.materialicons.IconDrawable;
import com.malinskiy.materialicons.Iconify;
import com.malinskiy.materialicons.LogUtils;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Button;
import ohos.agp.components.ComponentState;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.render.BlendMode;
import ohos.agp.render.ColorFilter;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    private Context context = null;
    private IconDrawable element = null;
    private Iconify.IconValue iconValue = null;

    @Before
    public void init() {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    }

    private String getCustomText(int position) {
        iconValue = Iconify.IconValue.values()[position];
        return iconValue.formattedName();
    }

    private IconDrawable getCustomIcon(int position) {
        Image iconView = new Image(context);
        element = new IconDrawable(iconView, iconValue);
        return element;
    }

    private IconDrawable getCustomIconButton(int position) {
        Button iconBtnView = new Button(context);
        element = new IconDrawable(iconBtnView, iconValue);
        Iconify.addIconsEditMode(iconBtnView);
        return element;
    }

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.malinskiy.materialicons", actualBundleName);
        LogUtils.d(getClass().getName(), "Test info：" + actualBundleName);
    }

    @Test
    public void testIconImageDrawable() {
        String txt = getCustomText(1);
        LogUtils.e(getClass().getName(), "Test info：" + txt);
        assertEquals("{zmdi_airplane_off}", txt);
        getCustomIcon(1);
        element
                .sizeRes(ResourceTable.Float_icon_detailed_size)
                .actionBarSize()
                .sizeVp(50)
                .sizePx(12)
                .alpha(10)
                .colorRes(ResourceTable.Color_detail_color)
                .color(ResourceTable.Color_detail_color)
                .setColorFilter(new ColorFilter(context.getColor(ResourceTable.Color_filter_color), BlendMode.COLOR));
        assertEquals(element.getBounds().getHeight(), element.getIntrinsicHeight());
        assertEquals(element.getBounds().getWidth(), element.getIntrinsicWidth());
        assertEquals(element.getBounds().right,12);
        assertEquals(element.getBounds().bottom, 12);
        element.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
        element.clearColorFilter();
        assertEquals(element.getOpacity(), 10);
        Text text = new Text(context);
        text.setText("text name");
        Iconify.addIcons(text);

    }

    @Test
    public void testIconButtonDrawable() {
        String text = getCustomText(1);
        assertEquals("{zmdi_airplane_off}", text);
        getCustomIconButton(1);
        element
                .sizeRes(ResourceTable.Float_icon_detailed_size)
                .actionBarSize()
                .sizeVp(40)
                .sizePx(5)
                .alpha(20)
                .colorRes(Color.RED.getValue())
                .color(Color.YELLOW.getValue())
                .setColorFilter(new ColorFilter(context.getColor(ResourceTable.Color_filter_color), BlendMode.COLOR));
        assertEquals(element.getBounds().getHeight(), element.getIntrinsicHeight());
        assertEquals(element.getBounds().getWidth(), element.getIntrinsicWidth());
        assertEquals(element.getBounds().right,5);
        assertEquals(element.getBounds().bottom, 5);
        element.clearColorFilter();
        element.setState(new int[]{ComponentState.COMPONENT_STATE_SELECTED});
        element.setStyle(Paint.Style.FILL_STYLE);
        assertEquals(element.getOpacity(), 20);
        assertEquals(element.getAlpha(), 255);
        LogUtils.i(getClass().getName(), "Test info：" + text);
    }
}